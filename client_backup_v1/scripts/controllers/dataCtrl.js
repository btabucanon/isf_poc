'use strict';

angular.module('angularApp')
  .controller('DataCtrl', function ($scope, $rootScope, $log, $q, DataServices, Model1MetadataResource) {

        $log.info("DataCtrl init");

        $scope.database_records = {};
        $scope.show_users_list = false;

        $scope.model_1_metadata_systems = DataServices.getModel1MetadataSystems();

        $scope.get_model_1_records = function() {
            $log.info("getting model 1 records");

            // var request_payload = {level_1:'metadata',level_2:'subsystems',level_3:'bysubsystemid',level_4:'1'};
            var request_payload = {level_1:'metadata',level_2:'attributes'};

            Model1MetadataResource.query(request_payload).$promise
            .then(function(response) {
                $scope.database_records = response;
            })
        };

        // $scope.get_database_records = function() {
        //     $log.info("getting database records (from dual)");
        //
        //     DatabaseRecordsResource.get().$promise
        //     .then(function(response) {
        //         $scope.database_records = JSON.stringify(response.response, null, 2);
        //     })
        // };
        //
        // $scope.get_users_records = function() {
        //     $log.info("getting users records (test.users)");
        //
        //     UsersRecordsResource.get().$promise
        //     .then(function(response) {
        //         $scope.users_records = response.response;
        //         $scope.show_users_list = true;
        //     })
        // };

	})

// Resolve
.constant('DataCtrlResolveMap', {
    // _staticText: function($log, DataServices) {
    //     return DataServices.getStaticText();
    // },
    // _intermediateHost: function($log, DataServices) {
    //     return DataServices.getIntermediateHost();
    // }
})