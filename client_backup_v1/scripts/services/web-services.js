'use strict';

angular.module('angularApp')

    .factory('Model1MetadataResource', function($resource) {

        var api_model_1_url = 'http://ec2-54-206-127-159.ap-southeast-2.compute.amazonaws.com:8080/api/:level_1/:level_2/:level_3/:level_4';

       return $resource(api_model_1_url, {},
        {
            'get' : {method: 'GET'},
            'query': { method: 'GET', isArray: true},
            'submit' : {method: 'POST'}
        })
    })