'use strict';

angular.module('angularApp')

.factory('DataServices', function ($resource, $q, $log, $http) {

var MODEL_1_METADATA_SYSTEMS = [{"SYSTEM_ID":1,"SYSTEM_SHORT_NAME":"WWI","SYSTEM_NAME":"WORLD WIDE IMPORTERS","ACRONYM_DEFINITION":"WORLD WIDE IMPORTERS","PURPOSE":"Wide World Imports platform is our wholesales logistics platform.  It covers Purchase orders, Sales, and Resource Management","CLASSIFICATION":"Management","DIVISION":"Group","SEGMENT":"Global","REGION":"Global","BUSINESS_UNIT":"Group","BUSINESS_OWNER":"Peter Brdar","SYSTEM_PLATFORM":"Windows","DATABASE_TECHNOLOGY":"SQL Server","CTRL_START_TS":"2010-01-01T00:00:00.000Z","CTRL_END_TS":"2999-12-31T00:00:00.000Z","CTRL_UPDATED_BY":"peterbrdar"}];
var MODEL_1_METADATA_SUBSYSTEMS_1 =  [{"SUB_SYSTEM_ID":1,"SYSTEM_ID":1,"SYSTEM_SHORT_NAME":"WWI","SUB_SYSTEM_SHORT_NAME":"WWI-APP","SUB_SYSTEM_NAME":"WWI - APPLICATION","ACRONYM_DEFINITION":"WORLD WIDE IMPORTERS - APPLICATION","PURPOSE":"Wide World Imports platform is our wholesales logistics platform. It covers Purchase orders, Sales, and Resource Management\r\n\tApplication sub system is used to manage the platform as a whole","DATA_STEWARD":"Brian Tabucanon","BUSINESS_SME":"Brian Tabucanon","TECHNICAL_SME":"Paulo Barbosa","RETENTION_PERIOD_IN_DAYS":7,"PROD_SUPPORT_GROUP_NAME":"WWI Support","PROD_SUPPORT_CONTACT_GRP_EMAIL":"wwi.support@servian.com","PROD_SUPPORT_PRIMARY_PHONE_NBR":"555 1234","PROD_SUPPORT_AVAIL_ON_WEEKDAYS":"Y","CTRL_START_TS":"2010-01-01T00:00:00.000Z","CTRL_END_TS":"2999-12-31T00:00:00.000Z","CTRL_UPDATED_BY":"peterbrdar"}];
var MODEL_2_METADATA_SYSTEMS = [{'key_1': 'value_1'}];

    return {
        getModel1MetadataSystems: function(){
            return MODEL_1_METADATA_SYSTEMS
        },
        getModel1MetadataSubSystems1: function(){
            return MODEL_1_METADATA_SUBSYSTEMS_1
        },
        getModel2MetadataSystems: function(){
            return MODEL_2_METADATA_SYSTEMS
        }

    }
});