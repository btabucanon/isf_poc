'use strict';

angular.module('angularApp')

    .factory('APIResource', function(_system_short_name) {

        var API_METADATA = [
            {
                "MODEL_NUMBER": 1,
                "SYSTEM_SHORT_NAME": "WWI",
                "API_URL": 'http://ec2-54-252-162-132.ap-southeast-2.compute.amazonaws.com:8080'
            },
            {
                "MODEL_NUMBER": 2,
                "SYSTEM_SHORT_NAME": "NWS",
                "API_URL": 'http://ec2-54-252-179-187.ap-southeast-2.compute.amazonaws.com:8080'
            }
        ];

        var api_metadata_index = _.findIndex(API_METADATA, {'SYSTEM_SHORT_NAME': _system_short_name});
        var selected_api_metadata = API_METADATA[api_metadata_index];

        return {
            getAPIURL: function () {
                return selected_api_metadata.API_URL
            }
        }

    })

    .factory('MetadataResource', function($resource, $log) {

        $log.info("Inside MetadataResource Factory");

        // var api_model_1_url = 'http://ec2-54-206-127-159.ap-southeast-2.compute.amazonaws.com:8080/api/:level_1/:level_2/:level_3/:level_4';
        // var api_model_2_url = 'http://ec2-54-252-179-187.ap-southeast-2.compute.amazonaws.com:8080/api/:level_1/:level_2/:level_3/:level_4';

        var api_model_1_url = 'http://ec2-54-252-162-132.ap-southeast-2.compute.amazonaws.com:8080/api/:level_1/:level_2/:level_3/:level_4';
        var api_model_2_url = 'http://ec2-54-252-179-187.ap-southeast-2.compute.amazonaws.com:8080/api/:level_1/:level_2/:level_3/:level_4';

        // var api_url = APIResource.getAPIURL('WWI')
        // var resource_url = api_url + '/api/:level_1/:level_2/:level_3/:level_4';

       return $resource(api_model_1_url, {},
        {
            'get' : {method: 'GET'},
            'query': { method: 'GET', isArray: true},
            'submit' : {method: 'POST'}
        })
    })

    .factory('DataResource', function($resource) {

        // var api_model_1_url = 'http://ec2-54-206-127-159.ap-southeast-2.compute.amazonaws.com:8080/api/:level_1/:level_2/:level_3/:level_4';
        // var api_model_2_url = 'http://ec2-54-252-179-187.ap-southeast-2.compute.amazonaws.com:8080/api/:level_1/:level_2/:level_3/:level_4';

        var api_model_1_url = 'http://ec2-54-252-162-132.ap-southeast-2.compute.amazonaws.com:8080/api/:level_1/:level_2/:level_3/:level_4';
        var api_model_2_url = 'http://ec2-54-252-179-187.ap-southeast-2.compute.amazonaws.com:8080/api/:level_1/:level_2/:level_3/:level_4';

       return $resource(api_model_1_url, {},
        {
            'get' : {method: 'GET'},
            'query': { method: 'GET', isArray: true},
            'submit' : {method: 'POST'}
        })
    })