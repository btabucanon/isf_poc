'use strict';

angular.module('angularApp', [
    'ngCookies',
    'ngResource',
    'ui.router',
    'ui.bootstrap'
])

    .config(function ($locationProvider, $httpProvider, $stateProvider, $urlRouterProvider, $injector) {


        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        $locationProvider.hashPrefix();

        /*
         ui-router state definitions
         */
        $stateProvider

            .state('home', {
                url: "/home",
                templateUrl: "views/home.html",
                controller: 'HomeCtrl',
                resolve: $injector.get('HomeCtrlResolveMap')
            })

            .state('help', {
                url: "/help",
                templateUrl: "views/help.html",
                // templateUrl: "http://ec2-54-206-127-159.ap-southeast-2.compute.amazonaws.com/help/",
                controller: 'AboutCtrl'
            })

            .state('er', {
                url: "/er",
                templateUrl: "views/er.html",
                controller: 'ERCtrl'
            })

            .state('lineage', {
                url: "/lineage",
                templateUrl: "views/lineage.html",
                controller: 'LineageCtrl'
            })

            .state('status', {
                url: "/status",
                templateUrl: "views/status.html",
                controller: 'StatusCtrl'
            })

            .state('other', {
                url: "/other",
                templateUrl: "views/other.html",
                controller: 'OtherCtrl'
            })

            .state('uam', {
                url: "/uam",
                templateUrl: "views/uam.html",
                controller: 'UAMCtrl'
            })

        $urlRouterProvider.otherwise("/home");


    }).run(function ($rootScope, $location, $log) {

        $rootScope.$on('$routeChangeError', function () {
            $log.error("$rootScope: got $routeChangeError event");
        });

    })

;

