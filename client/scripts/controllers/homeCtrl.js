'use strict';

angular.module('angularApp')
  .controller('HomeCtrl', function ($scope, $rootScope, $log, $q, DataServices, MetadataResource, DataResource, _systems) {

        $log.info("HomeCtrl init");

        $scope.get_metadata_button_disabled = true;
        $scope.get_data_button_disabled = true;

        $scope.systems = _systems;
        $scope.systems_test = {
            'system_id': _systems[0].SYSTEM_ID,
            'system_short_name': _systems[0].SYSTEM_SHORT_NAME,
            'system_name': _systems[0].SYSTEM_NAME,
            'acronym_definition': _systems[0].ACRONYM_DEFINITION,
            'purpose': _systems[0].PURPOSE,
            'classification': _systems[0].CLASSIFICATION,
            'division': _systems[0].DIVISION,
            'segment': _systems[0].SEGMENT,
            'region': _systems[0].REGION,
            'business_unit': _systems[0].BUSINESS_UNIT,
            'business_owner': _systems[0].BUSINESS_OWNER,
            'system_platform': _systems[0].SYSTEM_PLATFORM,
            'database_technology': _systems[0].DATABASE_TECHNOLOGY,
            'ctrl_start_ts': _systems[0].CTRL_START_TS,
            'ctrl_end_ts': _systems[0].CTRL_END_TS,
            'ctrl_updated_by':_systems[0].CTRL_UPDATED_BY
        };


        $scope.set_system = function(system_id) {

            var system_index = _.findIndex($scope.systems, {'SYSTEM_ID': system_id});
            $scope.selected_system = $scope.systems[system_index];

            var sub_system_payload = {
                // api_url: APIResource.getAPIURL($scope.systems.SYSTEM_SHORT_NAME),
                level_1: 'metadata',
                level_2: 'subsystems',
                level_3: 'bysystemid',
                level_4: system_id
            };

            $scope.get_metadata_button_disabled = false;
            $scope.get_data_button_disabled = true;

            $scope.selected_sub_system = false;
            $scope.selected_entity = false;
            $scope.selected_attribute = false;
            $scope.entities = false;
            $scope.attributes = false;

            MetadataResource.query(sub_system_payload).$promise
            .then(function(response) {

                // _.forEach(response, function(value) {
                //     $log.info("value: " + value);
                //    var sub_system = JSON.parse(value);
                //    $scope.sub_systems.push(sub_system)
                // });

                $scope.sub_systems = response;
            })

        };

        $scope.set_sub_system = function(sub_system_id) {

            var sub_system_index = _.findIndex($scope.sub_systems, {'SUB_SYSTEM_ID': sub_system_id});
            $scope.selected_sub_system = $scope.sub_systems[sub_system_index];

            var sub_system_payload = {
                level_1: 'metadata',
                level_2: 'entities',
                level_3: 'bysubsystemid',
                level_4: sub_system_id
            };

            $scope.get_metadata_button_disabled = false;
            $scope.get_data_button_disabled = true;

            $scope.selected_entity = false;
            $scope.selected_attribute = false;
            $scope.attributes = false;

            MetadataResource.query(sub_system_payload).$promise
            .then(function(response) {
                $scope.entities = response;
            })
        };

        $scope.set_entity = function(entity_id) {

            var entity_index = _.findIndex($scope.entities, {'ENTITY_ID': entity_id});
            $scope.selected_entity = $scope.entities[entity_index];

            var entity_payload = {
                level_1: 'metadata',
                level_2: 'attributes',
                level_3: 'byentityid',
                level_4: entity_id
            };

            $scope.get_metadata_button_disabled = false;
            $scope.get_data_button_disabled = false;

            $scope.selected_attribute = false;

            MetadataResource.query(entity_payload).$promise
            .then(function(response) {
                $scope.attributes = response;
            })
        };

        $scope.set_attribute = function(attribute_id) {

            var attribute_index = _.findIndex($scope.attributes, {'ATTRIBUTE_ID': attribute_id});
            $scope.selected_attribute = $scope.attributes[attribute_index];
        };

        // hardcoded API incase real API goes down during testing
        $scope.static_model_1_metadata_systems = DataServices.getModel1MetadataSystems();
        $scope.static_model_1_metadata_sub_systems_1 = DataServices.getModel1MetadataSubSystems1();

        $scope.get_metadata = function() {
            $log.info("get_metadata from webservice factory");

            var request_payload_static = {level_1:'metadata',level_2:'subsystems',level_3:'bysubsystemid',level_4:'1'};
            var request_payload_static_2 = {level_1:'metadata',level_2:'subsystems',level_3:'bysystemid',level_4:'1'};
            var request_payload_static_3 = {level_1:'metadata',level_2:'attributes'};

            var request_payload = {};

            if ($scope.selected_attribute) {
                request_payload.level_1 = 'metadata'; // TODO: make this dynamic later
                request_payload.level_2 = 'attributes';
                request_payload.level_3 = 'byattributeid';
                request_payload.level_4 = $scope.selected_attribute.ATTRIBUTE_ID;
                $scope.results_label = 'Attribute: ' + $scope.selected_attribute.PHYSICAL_NAME;
                $scope.show_results = true;
            }
            else if ($scope.selected_entity) {
                request_payload.level_1 = 'metadata'; // TODO: make this dynamic later
                request_payload.level_2 = 'entities';
                request_payload.level_3 = 'byentityid';
                request_payload.level_4 = $scope.selected_entity.ENTITY_ID;
                $scope.results_label = 'Entity: ' + $scope.selected_entity.PHYSICAL_NAME;
            }
            else if ($scope.selected_sub_system) {
                request_payload.level_1 = 'metadata'; // TODO: make this dynamic later
                request_payload.level_2 = 'subsystems';
                request_payload.level_3 = 'bysubsystemid';
                request_payload.level_4 = $scope.selected_sub_system.SUB_SYSTEM_ID;
                $scope.results_label = 'Sub System: ' + $scope.selected_sub_system.SUB_SYSTEM_NAME;
            }
            else if ($scope.selected_system) {
                request_payload.level_1 = 'metadata'; // TODO: make this dynamic later
                request_payload.level_2 = 'systems';
                $scope.results_label = 'System: ' + $scope.selected_system.SYSTEM_NAME;
            }

            MetadataResource.query(request_payload).$promise
            .then(function(response) {
                $scope.api_response = response;
                $scope.show_metadata_results = true;
                $scope.show_data_results = false;
                $scope.loading_metadata = false;
            })
        };

        $scope.get_data = function() {

            $log.info("get_data from webservice factory");

            var request_payload = {};

            if ($scope.selected_entity) {
                request_payload.level_1 = 'data'; // TODO: make this dynamic later
                request_payload.level_2 = $scope.selected_sub_system.PHYSICAL_NAME;
                request_payload.level_3 = $scope.selected_entity.PHYSICAL_NAME;
            }

            DataResource.query(request_payload).$promise
            .then(function(response) {
                $scope.api_response = response;
                $scope.api_response_limit10 = response.slice(0, 10);
                $scope.show_data_results = true;
                $scope.show_metadata_results = false;
                // $scope.results_label = 'Entity: ' + $scope.selected_entity.PHYSICAL_NAME;
                $scope.loading_data = false;
                $log.info("finished retrieving data")
            })

        };

        $scope.clear_selection = function() {

            $scope.selected_system = '';
            $scope.selected_sub_system = '';
            $scope.selected_entity = '';
            $scope.selected_attribute = '';
            $scope.results_label = '';
            $scope.show_metadata_results = false;
            $scope.show_data_results = false;
            $scope.sub_systems = '';
            $scope.entities = '';
            $scope.attributes = '';
            $scope.get_metadata_button_disabled = true;
            $scope.get_data_button_disabled = true;
        }

	})

// Resolve
.constant('HomeCtrlResolveMap', {
    // _staticText: function($log, DataServices) {
    //     return DataServices.getStaticText();
    // },
    // _intermediateHost: function($log, DataServices) {
    //     return DataServices.getIntermediateHost();
    // }
    _systems: function($log, MetadataResource) {
        return MetadataResource.query({level_1:'metadata',level_2:'systems'}).$promise
            .then(function(response) {
                return response;
            })
    }
})