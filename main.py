from flask import Flask, send_from_directory
from flask_restful import Api
import isf_api

app = Flask(__name__, static_folder="client")
api = Api(app)


@app.route('/')
def index():
    return 'Index Page'


@app.route('/<path:path>')
def serve_page(path):
    return send_from_directory('client', path)


@app.route('/hello')
def hello_world():
    return 'Hello World!'


api.add_resource(isf_api.HelloWorld, '/test')
api.add_resource(isf_api.ApiHandler, '/api/data')


if __name__ == '__main__':
    app.run()
