
SOMETHING = {'hello': 'world'}

DUMMY_DATA = {
    "name": "Bruce Banner",
    "favourite_colour": "green",
    "test_nested" : [
        {"key_1": "value_1"},
        {"key_2": "value_2"},
        {"key_3": "value_3"}
    ]
}