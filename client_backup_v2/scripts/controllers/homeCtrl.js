'use strict';

angular.module('angularApp')
  .controller('HomeCtrl', function ($scope, $rootScope, $log, $q, DataServices, MetadataResource, _systems) {

        $log.info("HomeCtrl init");

        $scope.systems = _systems;
        $scope.systems_test = {
            'system_id': _systems[0].SYSTEM_ID,
            'system_short_name': _systems[0].SYSTEM_SHORT_NAME,
            'system_name': _systems[0].SYSTEM_NAME,
            'acronym_definition': _systems[0].ACRONYM_DEFINITION,
            'purpose': _systems[0].PURPOSE,
            'classification': _systems[0].CLASSIFICATION,
            'division': _systems[0].DIVISION,
            'segment': _systems[0].SEGMENT,
            'region': _systems[0].REGION,
            'business_unit': _systems[0].BUSINESS_UNIT,
            'business_owner': _systems[0].BUSINESS_OWNER,
            'system_platform': _systems[0].SYSTEM_PLATFORM,
            'database_technology': _systems[0].DATABASE_TECHNOLOGY,
            'ctrl_start_ts': _systems[0].CTRL_START_TS,
            'ctrl_end_ts': _systems[0].CTRL_END_TS,
            'ctrl_updated_by':_systems[0].CTRL_UPDATED_BY
        };


        $scope.set_system = function(system_id) {
            $scope.selected_system_id = system_id;

            var system_index = _.findIndex($scope.systems, {'SYSTEM_ID': system_id});
            $scope.selected_system = $scope.systems[system_index];

        };

        // hardcoded API incase real API goes down during testing
        $scope.model_1_metadata_systems = DataServices.getModel1MetadataSystems();

        $scope.call_api = function() {
            $log.info("calling api from webservice factory");

            var request_payload = {level_1:'metadata',level_2:'subsystems',level_3:'bysubsystemid',level_4:'1'};
            // var request_payload = {level_1:'metadata',level_2:'attributes'};

            MetadataResource.query(request_payload).$promise
            .then(function(response) {
                $scope.api_response = response;
            })
        }


	})

// Resolve
.constant('HomeCtrlResolveMap', {
    // _staticText: function($log, DataServices) {
    //     return DataServices.getStaticText();
    // },
    // _intermediateHost: function($log, DataServices) {
    //     return DataServices.getIntermediateHost();
    // }
    _systems: function($log, MetadataResource) {
        return MetadataResource.query({level_1:'metadata',level_2:'systems'}).$promise
            .then(function(response) {
                return response;
            })
    }
})