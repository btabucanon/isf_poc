'use strict';

angular.module('angularApp', [
    'ngCookies',
    'ngResource',
    'ui.router',
    'ui.bootstrap'
])

    .config(function ($locationProvider, $httpProvider, $stateProvider, $urlRouterProvider, $injector) {


        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        $locationProvider.hashPrefix();

        /*
         ui-router state definitions
         */
        $stateProvider

            .state('home', {
                url: "/home",
                templateUrl: "views/home.html",
                controller: 'HomeCtrl',
                resolve: $injector.get('HomeCtrlResolveMap')
            })

            .state('help', {
                url: "/help",
                templateUrl: "views/help.html",
                controller: 'AboutCtrl'
            })

        $urlRouterProvider.otherwise("/home");


    }).run(function ($rootScope, $location, $log) {

        $rootScope.$on('$routeChangeError', function () {
            $log.error("$rootScope: got $routeChangeError event");
        });

    })

;

